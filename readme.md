# NewsSource
## Sample Project

This repository is for a brand new project that is underway. It is a Laravel 5.2.x app that pulls the latest news from popular sites. It is currently fetching news through the use of a Laravel Command that fetches news through RSS feeds while storing data with MySQL (Utilizing Eloquent) & S3 for image file storage.