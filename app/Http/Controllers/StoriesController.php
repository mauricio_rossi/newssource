<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Story;
use App\Models\NewsSource;

class StoriesController extends Controller
{
    /**
     * Serve up latest stories and offer filtering
     *
     * @param  Request  $request
     * @return Response
     */
    public function getStories(Request $request) {
        $order = $request->input('order', 'desc');
        $stories = Story::orderBy('id', $order);
        $keyword = $request->input('keyword');
        if ($keyword) {
            $stories->where('title', 'like', '%' . $keyword . '%')
                    ->orWhere('description', 'like', '%' . $keyword . '%');
        }
        $stories = $stories->paginate(40);
        $sources = NewsSource::all();
        return view('stories', [
            'stories' => $stories,
            'keyword' => $keyword,
            'order' => $order,
            'sources' => $sources
        ]);
    }

    /**
     * Serve up stories by source
     *
     * @param  Request  $request
     * @return Response
     */
    public function viewBySource(string $sourceId, Request $request) {
        $order = $request->input('order', 'desc');
        $stories = NewsSource::find($sourceId)
                    ->stories()
                    ->paginate(40);
        $sources = NewsSource::all();
        return view('stories', [
            'stories' => $stories,
            'keyword' => '',
            'order' => $order,
            'sources' => $sources
        ]);
    }
}