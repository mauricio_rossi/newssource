<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use \GuzzleHttp\Client;
use \App\Models\NewsSource;
use \App\Models\Story;
use \Storage;

class NewsScraper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:scraper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes popular news feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $sources = NewsSource::all();
        foreach($sources as $source) {
            $response = $this->client->get($source->rss_path);
            $xml = new \SimpleXMLElement($response->getBody()->getContents());
            $namespaces = $xml->getNamespaces(true);
            foreach($xml->channel->item as $xmlItem) {
                if ($this->isValidGuid($xmlItem->guid)) {
                    Log::warning('Receiving invalid data');
                } else {
                    if ($this->shouldAddStory($source->id, $xmlItem->guid)) {
                        $imgSrc = '';
                        if ($namespaces['media']) {
                            $media = $xmlItem->children($namespaces['media'])->group;
                            $content = $media->content;
                            if ($content) {
                                $imgSrc = $content->attributes()->url;
                                $imgSrc = $this->uploadToS3($imgSrc);
                            }
                        }
                        $story = new Story([
                            'news_source_id' => $source->id,
                            'news_source_story_guid' => $xmlItem->guid,
                            'url' => $xmlItem->link,
                            'title' => $xmlItem->title,
                            'description' => $xmlItem->description,
                            'image' => $imgSrc
                        ]);
                        $story->save();
                    }
                }
            }
        }
    }

    /**
     * Uploads to S3 and returns the new path
     * 
     * @return string
    */
    private function uploadToS3(string $path) : string {
        // Saving to disk to reduce memory usage
        $tmpFileName = tempnam('/tmp', 'news');
        $this->client->get($path,
        [
            'save_to' => $tmpFileName,
        ]);
        $filename = $this->getFilename();
        $tmpFile = fopen($tmpFileName, 'r');
        \Storage::disk('s3')->put($filename, $tmpFile);
        fclose($tmpFile);
        return Storage::url($filename);
    }

    /**
     * Returns a unique file name
     * 
     * @return string
    */
    private function getFilename() : string {
        do {
            $filename = uniqid() . '.jpg';
        }
        while(\Storage::disk('s3')->exists($filename));
        return $filename;
    }

    /**
     * Determine whether news story should be stored by using external's site's GUID.
     *
     * @return bool
     */
    private function shouldAddStory(int $sourceId, string $storyGuid) : bool {
        $existingStory = Story::where('news_source_id', $sourceId)
                            ->where('news_source_story_guid', $storyGuid)
                            ->first();
        if ($existingStory) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Determine whether RSS's feed guid is valid.
     *
     * @return bool
     */
    private function isValidGuid(string $guid) : bool {
        if (preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/', $guid)) {
          return true;
        } else {
          return false;
        }
    }
}
