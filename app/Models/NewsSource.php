<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsSource extends Model
{
    /**
     * Get the stories for this source.
     */
    public function stories()
    {
        return $this->hasMany('App\Models\Story');
    }
}
