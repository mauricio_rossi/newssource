<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    /**
     * Specifying table.
     * @var string
     */
    protected $table = 'stories';

    /**
     * Specifying fillable fields
     * @var array
     */
    protected $fillable = [
        'news_source_id',
        'news_source_story_guid',
        'title',
        'description',
        'url',
        'image'
    ];
}
