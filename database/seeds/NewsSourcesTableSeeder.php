<?php

use Illuminate\Database\Seeder;

class NewsSourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_sources')->insert([
            [
                'name' => 'CNN',
                'domain' => 'cnn.com',
                'rss_path' => 'http://rss.cnn.com/rss/cnn_topstories.rss'
            ]
        ]);
    }
}
