<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>NewsSource</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/readable/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div role="navigation" class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header"><a href="/" class="navbar-brand">NewsSource</a></div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        @foreach($sources as $source)
                        <li><a href="{{ url('/source/' . $source->id) }}">{{ $source->domain }}</a></li>
                        @endforeach
                    </ul>
                    <div class="pull-right">
                        <div class="news-form">
                            {{ Form::open(array('action' => 'StoriesController@getStories', 'method' => 'GET')) }}
                                {{ Form::text('keyword', $keyword, array('placeholder' => 'Enter search', 'class' => 'form-control')) }}
                                {{ Form::select('order', ['asc' => 'Oldest First', 'desc' => 'Newest First'], $order, array('class' => 'form-control')) }}
                                {{ Form::submit('Send', array('class' => 'news-form__submit btn btn-primary')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid clearfix">
            @if (count($stories) > 0)
                @foreach ($stories as $story)
                    <div class="story-item col-xs-4">
                        <div class="story-item__content">
                            <a href="{{ $story->url }}">
                                <img src="{{ $story->image }}" class="story-item__image">
                                <h3 class="story-item__title">{{ $story->title }}</h3>
                            </a>
                        </div>
                    </div>
                @endforeach
            @else 
            <div class="col-xs-12">
                <div class="alert alert-warning">No results found</div>
            </div>
            @endif
        </div>
        <div class="stories-pagination">
            <div class="col-xs-12">
                {!! $stories->render() !!}
            </div>
        </div>
    </body>
</html>